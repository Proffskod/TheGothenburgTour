- Finished the javadocs. [likfarmenhet]

- Updated the javadoc. [sanderdan]

- Merge branch 'javadoc' [likfarmenhet]

- Fixed and cleaned quizes. [likfarmenhet]

- Added some javadoc to the interfaces. [ForsmanFredrik]

- Git added the class i forgot. [ForsmanFredrik]

- Refactored classes, inheritdoc and interface update. [ForsmanFredrik]

- Merge branch 'javadoc' [likfarmenhet]

- For merge, javadoc. [likfarmenhet]

- Saving som small changes to bransh. [likfarmenhet]

- Fixed partial javadoc, commit for saving current changes.
  [likfarmenhet]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Update readme.md. [Sander Danielsson]

- Minor update. [Sander Danielsson]

- More format updates. [Sander Danielsson]

- More format changes. [Sander Danielsson]

- Format update. [Sander Danielsson]

- Merge branch 'master' of
  https://gitlab.com/Proffskod/TheGothenburgTour. [sanderdan]

- Formation. [Sander Danielsson]

- Readme added. [sanderdan]

- Changelog added. [sanderdan]

- Last changes to score respond. [ForsmanFredrik]

- Merge branch 'testing' [ForsmanFredrik]

- Some more testing. [ForsmanFredrik]

- Merge branch 'master' of
  https://gitlab.com/Proffskod/TheGothenburgTour into savinglandmarks.
  [sanderdan]

- Merge branch 'unittesting' [ForsmanFredrik]

- Fully working score and landmark saving+reset. [sanderdan]

- Merge branch 'fixscore' of
  https://gitlab.com/Proffskod/TheGothenburgTour into resultscreen.
  [sanderdan]

- Writing to file works. [likfarmenhet]

- Trying some stuff. [likfarmenhet]

- Merge branch 'unittesting' of
  https://gitlab.com/Proffskod/TheGothenburgTour into resultscreen.
  [sanderdan]

- Unittesting. [ForsmanFredrik]

- Point system added. [sanderdan]

- Fixed last bug. [likfarmenhet]

- Last fixing. [ForsmanFredrik]

- Fixed some minor bugs. [ForsmanFredrik]

- Not working on sander genymotion. [sanderdan]

- Styled the theme. [ForsmanFredrik]

- Merge branch 'snippet' [likfarmenhet]

- Fixed setInfo for snippets. [likfarmenhet]

- A little small fix. [ForsmanFredrik]

- Fixed navigationdrawer and homepage, added how to play function.
  [likfarmenhet]

- Fixed notifications. [likfarmenhet]

- Merge branch 'master' of
  https://gitlab.com/Proffskod/TheGothenburgTour. [likfarmenhet]

- Merge branch 'circlequestion' [ForsmanFredrik]

- Fixed all the circle, question bugs. [ForsmanFredrik]

- Fixed logo to app for all devices and resolution, also for
  notification. [likfarmenhet]

- Fixed onstart complete. [likfarmenhet]

- Fixed onstart. [likfarmenhet]

- Added portraitorientation. [ForsmanFredrik]

- Merge branch 'shoptour' [likfarmenhet]

- Adding shoppingtour to fredriks merge. [likfarmenhet]

- Major fix with the questions, circle and much more. [ForsmanFredrik]

- Fixed mergeconflict. [likfarmenhet]

- Quickfix with the Questionfrag and Shoppingtour. [likfarmenhet]

- Removed unused variable, structured code. [ForsmanFredrik]

- Merge branch 'master' of
  https://gitlab.com/Proffskod/TheGothenburgTour. [likfarmenhet]

- Merge branch 'new-directions' of
  https://gitlab.com/Proffskod/TheGothenburgTour. [sanderdan]

- Added directions for all walks, just need to change some orders of
  some landmarks. [sanderdan]

- Circle fixed. [likfarmenhet]

- Added a start page and the functionality. [ForsmanFredrik]

- Merge branch 'startpage' [likfarmenhet]

- Created a startpage. [likfarmenhet]

- Fixed so shopping and touristquiz works. [ForsmanFredrik]

- Fixed the fragments. [sanderdan]

- Fixed mergeconflicts. [likfarmenhet]

- Changed what happenes when you selecta an answer. [likfarmenhet]

- Fixed the circle in all classes. [ForsmanFredrik]

- Fixed mergeconflicts. [ForsmanFredrik]

- Fixed color of the circle on new location. [ForsmanFredrik]

- Not very precise, but it works. [sanderdan]

- Merge branch 'question-fixing' [sanderdan]

- Minor changes in some questions. [sanderdan]

- Skapat QuestionShoppingMockup och lagt till frågor samt svar.
  [likfarmenhet]

- Questions now have an answer. [sanderdan]

- Fixed merge conflicts. [ForsmanFredrik]

- Fixed Touristquiz. [ForsmanFredrik]

- Some style changes in questionfragment xml stuff. [sanderdan]

- Merge branch 'notification' [ForsmanFredrik]

- Fixed a notificationmanager. [ForsmanFredrik]

- No questionfragmentadater needed, but this is ready for test.
  [sanderdan]

- A simple QuestionFragment is now working, check SlottskogenQuiz for
  more info. [sanderdan]

- Mainactivity unit test. [ForsmanFredrik]

- Test. [likfarmenhet]

- Question class + mockup, started with a question fragment. [sanderdan]

- Fixed the fragments. [ForsmanFredrik]

- Merge branch 'famousplaces' [ForsmanFredrik]

- Tourist Quizwalk. [ForsmanFredrik]

- Merge branch 'shoppingtour' [likfarmenhet]

- Fixat en shopping-tipspromenad med 10platser. [likfarmenhet]

- First commit for slottskogen quiz walk. [ForsmanFredrik]

- Added 15 places. [ForsmanFredrik]

- Added a icon. [ForsmanFredrik]

- Test. [likfarmenhet]

- Adding the ignore. [ForsmanFredrik]

- Removing ignore. [ForsmanFredrik]

- Alex, alex, alex... [ForsmanFredrik]

- Adding the gitignore. [ForsmanFredrik]

- Initial commit. [ForsmanFredrik]
