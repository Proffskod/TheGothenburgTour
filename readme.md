# The Gothenburg Walk &nbsp;
The Gothenburg Walk is an application for Android phones and tablets (minimum Android 4.1 "Jelly Bean").  
It's a great application for someone who wants to know more about  
Gothenburg or someone who just wants to add something to their walk.
## Version 1.1 &nbsp;
<a href="http://imgur.com/y2AUjUO"><img src="http://i.imgur.com/y2AUjUOm.jpg?1" title="source: imgur.com" /></a>
<a href="http://imgur.com/E9fhgZ3"><img src="http://i.imgur.com/E9fhgZ3m.jpg" title="source: imgur.com" /></a>
<a href="http://imgur.com/miwbldD"><img src="http://i.imgur.com/miwbldDm.jpg?1" title="source: imgur.com" /></a>

---
# Installation &nbsp;
#### 1. &nbsp; Download the latest apk from: https://www.dropbox.com/s/tcrnjrtzjma8yn8/app-release.apk?dl=0

#### 2. &nbsp; **If you have an Android phone/tablet, jump to step 5.** If you don't have an android phone, go to step 4.

#### 3. &nbsp; If you don't have an Android emulator, we recommend to use Genymotion for our app.  Simply just drag the .apk into your emulator and the emulator will  install the app. &nbsp;

#### 4. &nbsp; Follow these simple steps:  
How To Allow Installation Of External Apps On Your Android Phone

	4.1
	Access the settings menu on your Android phone.
	Most phones have an external menu button that when pressed allows you to Select Settings.  
	However, you can always access settings from the app drawer.

	4.2
	Tap Applications(On HTC devices applications may be known as Programs).

	4.3
	Tap Unknown Sources, allow install of non market applications from unknown sources
	
	4.4
	An Attention pop-up will appear. It just warns you that third-party apps can  
	sometimes be unsafe or contain viruses, but as long as you only install them  
	from trusted developers you should be fine. Click OK.  
	Done!

	Android is now set up to allow install of non-Market applications!  
	
	
Now you can install any app from anywhere that you want, 
**but do use common sense and make sure you do some background checking when  
downloading from an unknown source.**

#### 5. &nbsp; Go to the url provided in step 1 and download the .apk file. 
Then find the file in your phone and click on it, voíla! Your android device should now install the app if you have done everything as listed above.

#### 6. &nbsp; Start the app by clicking on the icon.

## API References &nbsp;
[Google Maps API](https://developers.google.com/maps/documentation/android/)  
[Google Maps Directions API](https://developers.google.com/maps/documentation/directions/)

## Links &nbsp;
[Changelog](https://gitlab.com/Proffskod/TheGothenburgTour/blob/master/changelog.md)

### #Proffskod Team Members
[sanderdan](https://gitlab.com/u/sanderdan)  
[forsamano](https://gitlab.com/u/forsamano)  
[likfarmenhet](https://gitlab.com/u/likfarmenhet)

## License
[MIT](http://opensource.org/licenses/MIT)