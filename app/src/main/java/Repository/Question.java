package Repository;

import java.util.Arrays;

/**
 * A Question i the application
 */
public class Question {

    private int questionId;
    private int quizWalkId;
    private String question;
    private String[] answers;
    private int rightAnswer;

    /**
     * Initializes a Question
     *
     * @param questionId the id of the question
     * @param quizWalkId the id of the quizwalk
     * @param question the actual question
     * @param answer1 the first answer of the question
     * @param answer2 the second answer of the question
     * @param answer3 the third answer of the question
     * @param rightAnswer the correct answer to the question
     */
    public Question(int questionId, int quizWalkId, String question, String answer1, String answer2, String answer3, int rightAnswer) {
        this.questionId = questionId;
        this.quizWalkId = quizWalkId;
        this.question = question;
        this.rightAnswer = rightAnswer;
        answers = new String[]{answer1, answer2, answer3};
    }

    public int getQuestionId() {
        return questionId;
    }

    public int getQuizWalkId() {
        return quizWalkId;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String getQuestion() {
        return question;
    }

    public int getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(int rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    /**
     * @return a String representation of the question
     */
    @Override
    public String toString() {
        return "Question{" +
                "questionId=" + questionId +
                ", quizWalkId=" + quizWalkId +
                ", question='" + question + '\'' +
                ", answers=" + Arrays.toString(answers) +
                '}';
    }

}