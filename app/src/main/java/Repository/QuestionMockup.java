package Repository;

/**
 * A mockup for the quiz that contains {@code Question}'s
 */
public interface QuestionMockup {

    /**
     * gets a question by its id and returns it
     * @param id the id of the question
     * @return the question by it's id
     */
    public Question getQuestionById(int id);

    /**
     * Creates all the question in the quiz and adds them
     */
    public void createQuestions();
}
