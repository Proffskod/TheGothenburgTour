package Repository;

import java.util.ArrayList;

/**
 * Mockup for a Question in the Shopping quizwalk
 */
public class QuestionsShoppingMockup implements QuestionMockup {

    private ArrayList<Question> questions = new ArrayList<>();
    private Question question;

    /**
     * {@inheritDoc}
     */
    @Override
    public Question getQuestionById(int id) {
        if (questions.isEmpty()) {
            createQuestions();
        }
        for (Question q : questions) {
            int i = q.getQuestionId();
            if (i == id) {
                question = q;
            }
        }
        return question;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createQuestions() {
        Question carlings = new Question(1, 3, "Det som tidigare kallats för denim användes primärt utav arbetare " +
                "från brancher som bygg och sjöfart. Vilket år började rebeliska ungdomar " +
                "i USA använda vad dom började kalla för jeans?",
                "år 1960", "år 1950", "år 1940", 2);
        Question wassens = new Question(2, 3, "Handväskan är idag den populäraste accessoaren bland kvinnor, men förr " +
                "var det män som bar handväska. Man började man sy fickor på mäns kläder " +
                "vilket gjorde att kvinnorna blev ensama om att använda handväskan. " +
                "Under vilket århundrade blev handväskan till majoriteten kvinnans?",
                "1700-talet", "1800-talet", "1900-talet", 1);
        Question nordstansguld = new Question(3, 3, "Människan har fascinerats av diamanter i över 3000 år! De första hittades " +
                "i Indien, förmodligen var det framförallt den överlägsna hårdheten som gjorde " +
                "diamanter till nummer ett bland alla ädelstenar. Diamanter finns i många former " +
                "och klarhet. Vilken är den finaste klarheten en diamant kan ha?",
                "SI", "VVS", "IF", 3);
        Question gustavadolfstorg = new Question(4, 3, "Göteborg grundades utav kung Gustav II Adolf. " +
                "Det som tidigare kallade Stora torget eller Stortorget blev Gustav Adolfs torg efter att en staty " +
                "rests i konugens ära. Vilket år restes Bengt Erland Fogelbergs staty utav Gustav II Adolf?",
                "år 1812", "år 1836", "år 1854", 3);
        Question lejontrappan = new Question(5, 3, "Vid Brunnsparken i Göteborg finns Lejontrappan. " +
                "Namet ärvdes från Lejonbron som bron tidigare hette. Trappan går ner i Göta kanal som rinner " +
                "igenom Göteborg. Hur långt sträcker sig Göta kanal?",
                "174km", "190km", "201km", 2);
        Question naturkompaniet = new Question(6, 3, "Naturkompaniet är en svensk detaljhandelskedja som säljer " +
                "friluftskläder och friluftsutrustning. Företaget grundades 1931 av den svenska scoutrörelsen. " +
                "Männsikor har i alla århundraden utforskat naturen och många än idag tänjer på sina gränser. " +
                "Hur länge klarar sig en vuxen människa i en överlevnadsituation på bara vatten?",
                "3 dagar", "4 dagar", "5 dagar", 1);
        Question nk = new Question(7, 3, "Nordiska kompaniet, känt som NK grundades ursprungligen 1864 av köpmannen " +
                "Ferdinand Lundquist. Idag är bolaget ägt utav Hufvudstaden AB. " +
                "Vad hette varuhuset Ferdinand Lundquist öppnade?",
                "F. Lundquist Boutique", "Boutique Ferdinand & Co.", "Ferd. Lundquist & Co.", 3);
        Question mq = new Question(8, 3, "År 1957 inleder 25 herr- och dambutiker ett samarbete under namnet Detex AB. " +
                "Huvudkontoret låg först i Stockholm sen Borås och nu Göteborg. 1988 ändrades " +
                "namnet till MQ. Christina Ståhl är VD för bolaget idag. När blev Christina Ståhl VD för MQ?",
                "år 1999", "år 2007", "år 2013", 3);
        Question zara = new Question(9, 3, "Det har sägs att Zara enbart behöver två veckor för att utveckla en ny produkt " +
                "och leverera den till affärer, jämfört med ett snitt i branschen på sex månader, " +
                "och lanserar runt 10 000 produkter om året sk. fast fashion. Från vilket land kommer företaget Zara?",
                "Venezuela", "Spanien", "Peru", 2);
        Question akademibokhandeln = new Question(10, 3, "Akademibokhandeln startade vid Stockholms universitet på " +
                "1970-talet. Det hette då Nordiska Bokhandelsgruppen och är idag Sveriges marknadsledande bokhandelskedja \n" +
                "med närmare 130 butiker spridda över hela landet. Hur mycket omsätter Akademibokandeln per år?",
                "ca 1200 miljoner kr", "ca 1300 miljoner kr", "ca 1400 miljoner kr", 2);

        questions.add(carlings);
        questions.add(wassens);
        questions.add(nordstansguld);
        questions.add(gustavadolfstorg);
        questions.add(lejontrappan);
        questions.add(naturkompaniet);
        questions.add(nk);
        questions.add(mq);
        questions.add(zara);
        questions.add(akademibokhandeln);
    }

}