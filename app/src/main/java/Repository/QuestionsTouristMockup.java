package Repository;

import java.util.ArrayList;

/**
 * Mockup for a Question in the Tourist quizwalk
 */
public class QuestionsTouristMockup implements QuestionMockup {

    private ArrayList<Question> questions = new ArrayList<>();
    private Question question;

    /**
     * {@inheritDoc}
     */
    @Override
    public Question getQuestionById(int id) {
        if (questions.isEmpty()) {
            createQuestions();
        }
        for (Question q : questions) {
            int i = q.getQuestionId();
            if (i == id) {
                question = q;
            }
        }
        return question;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createQuestions() {
        Question liseberg = new Question(1, 1, "Vem av dessa kända göteborgare har en egen värld, med t.ex ett café och " +
                "scen uppkallad efter sig, inne på nöjesparken?",
                "Ingvar Oldsberg", "Evert Taube", "Håkan Hellström", 2);
        Question universeum = new Question(2, 1, "Inne på Universeum finns ett enormt akvarium som rymmer över tre miljoner liter vatten" +
                "och hundratals olika arter. En av de populäraste aktiveterna i Akvariehallen är" +
                "hajmatningen, så till frågan, vilken är världens största hajart?",
                "Valhajen", "Vithajen", "Hammarhajen", 1);
        Question gothia = new Question(3, 1, "Det tredje tornet stod färdigt i februari 2015 med en höjd på 100 meter och fullbordade hotellet. " +
                "Inne på hotellet finns ett antal olika restauranger, vilken restaurang ligger inte i byggnaderna?",
                "Heaven 23", "Incontro", "Orgia", 3);
        Question scandinavium = new Question(4, 1, "Under stora delar av året spelar stadens Hockeylag Frölunda inne på arenan, vilket smeknamn " +
                "går laget under?",
                "Lakers", "Cowboys", "Indians", 3);
        Question ullevi = new Question(5, 1, "Den 7 juni år 2014 slogs publikrekordet på Ullevi av stadens egna Håkan Hellström " +
                "med 69 349 åskådare, vilken evenemang ligger tvåa på samma lista?",
                "Bruce Springsteen, 28 juli 2012", "Ingemar Johansson vs. Eddie Machen, 14 september 1958", "David Bowie, juni 1983", 1);
        Question heden = new Question(6, 1, "Ungdomsturneringen Gothia cup har sin bas på hedens fotbollsplaner, vilket av stadens lag " +
                "arrangerar turneringen?",
                "Gais", "BK Häcken", "IFK Göteborg", 2);
        Question tradgard = new Question(7, 1, "Under vilket århundrade bildades parken efter beslut av kung " +
                "Karl XIV Johan?",
                "1800-talet", "1900-talet", "1700-talet", 1);
        Question kopparmarra = new Question(8, 1, "Vilken svensk kung är det som statyn avbildar?",
                "Karl X", "Gustav II Adolf", "Karl IX", 3);
        Question rohsska = new Question(9, 1, "Röhsska museet invigdes 1916 och var ursprungligen friliggande, men byggdes samman " +
                "med en föreningsskola 1964, vilken förening byggdes den samman med?",
                "Slöjdföreningen", "Teaterföreningen", "Ingenjörsföreningen", 1);
        Question gotaplatsen = new Question(10, 1, "Högst upp på Avenyn paradgata ligger Götaplatsen och i dess center står statyn " +
                "som avbildar den grekiske guden Poseidon, statyn var tänkt att ha en annat namn, " +
                "nämligen efter den romerske motsvarigheten till Poseidon, vad heter den romerska " +
                "havsguden?",
                "Eros", "Dionysos", "Neptunus", 3);
        Question kiosken = new Question(11, 1, "Hur många bultar har Ölandsbron?", "sex miljoner", "sju miljoner", "fyra miljoner", 2);
        Question dahls = new Question(12, 1, "Vad kostar en god kaffe här på Dahls?", "10kr", "11kr", "12kr", 3);

        questions.add(liseberg);
        questions.add(universeum);
        questions.add(gothia);
        questions.add(scandinavium);
        questions.add(ullevi);
        questions.add(heden);
        questions.add(tradgard);
        questions.add(kopparmarra);
        questions.add(rohsska);
        questions.add(gotaplatsen);
        questions.add(kiosken);
        questions.add(dahls);
    }

}
