package Repository;

import java.util.ArrayList;

/**
 * Mockup for a Question in the Slottskogen quizwalk
 */
public class QuestionsSlottskogenMockup implements QuestionMockup {

    private ArrayList<Question> questions = new ArrayList<>();
    private Question question;

    /**
     * {@inheritDoc}
     */
    @Override
    public Question getQuestionById(int id) {
        if (questions.isEmpty()) {
            createQuestions();
        }
        for (Question q : questions) {
            int i = q.getQuestionId();
            if (i == id) {
                question = q;
            }
        }
        return question;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createQuestions() {
        Question linneplatsen = new Question(1, 1, "Vilket år rullade den första spårvagnen i Göteborg?",
                "1871", "1879", "1886", 2);
        Question minigolf = new Question(2, 1, "Hur många slag har man på sig att få ner golfbollen i hålet?",
                "5", "6", "7", 3);
        Question gamlaPolisVakten = new Question(3, 1, "Hur många pliktkast får man om man kastar frisbeen out of bounds?",
                "1", "2", "3", 1);
        Question villaBellparc = new Question(4, 1, "På tal om parker, vilken annan känd park ligger på Manhattan i New York?",
                "North Park", "West Park", "Central Park", 3);
        Question salDammen = new Question(5, 1, "Vilken typ av säl finner du i säldammen?",
                "Gråsäl", "Knubbsäl", "Vikaren", 2);
        Question azeleadalen = new Question(6, 1, "Vilken känd svensk artist sjunger om Azeleadalen?",
                "Evert Taube", "Håkan Hellström", "Veronica Maggio", 2);
        Question hagarna = new Question(7, 1, "Slottskogens Djurpark är en av Svergies äldsta, när öppnades den?",
                "1876", "1893", "1905", 1);
        Question barnensZoo = new Question(8, 1, "Vad kallas Grisarnas barn?",
                "Kalv", "Kut", "Kulting", 3);
        Question plikta = new Question(9, 1, "Hur lång är den längsta rutschbanan i Plikta?",
                "25", "30", "20", 1);
        Question naturhistoriska = new Question(10, 1, "Inne på Naturhistoriska Muséet kan du bland annat hitta dinosaurier. Vilken av dessa dinosaurier var en köttätare?",
                "Stegosaurie", "Tyrannosaurus Rex", "Triceratops", 2);

        questions.add(linneplatsen);
        questions.add(minigolf);
        questions.add(gamlaPolisVakten);
        questions.add(villaBellparc);
        questions.add(salDammen);
        questions.add(azeleadalen);
        questions.add(barnensZoo);
        questions.add(plikta);
        questions.add(naturhistoriska);
        questions.add(hagarna);
    }

}