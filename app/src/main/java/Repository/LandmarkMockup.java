package Repository;

import java.util.ArrayList;

/**
 * A mockup for the quiz that contains {@code Landmark}'s
 */
public interface LandmarkMockup {

    /**
     * Gets all the landmarks created
     * @return all landmarks created
     */
    public ArrayList<Landmark> getLandmarks();

    /**
     * Creates all the landmark in the quiz and adds them
     */
    public void createLandmarks();
}
