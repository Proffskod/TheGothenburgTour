package Repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Mockup for Landmarks in Shopping quiz walk.
 */
public class LandmarkShoppingMockup implements LandmarkMockup {

    private ArrayList<Landmark> shoppingLandmarks = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<Landmark> getLandmarks() {
        if (shoppingLandmarks.isEmpty()) {
            createLandmarks();
        }
        return shoppingLandmarks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createLandmarks() {
        Landmark carlings = new Landmark(1, "Carlings", new LatLng(57.708791, 11.970151));
        carlings.setInfo("populär klädaffär för ungdommar");
        Landmark wassens = new Landmark(2, "Wasséns", new LatLng(57.708478, 11.970901));
        wassens.setInfo("etablerades 1868");
        Landmark nordstansguld = new Landmark(3, "Nordstans Guld", new LatLng(57.707775, 11.968428));
        nordstansguld.setInfo("årets guldsmedsbutik 2012");
        Landmark gustavadolf = new Landmark(4, "Gustavs Adolfs torg", new LatLng(57.707089, 11.9671801));
        gustavadolf.setInfo("tidigare kallat Stortorget");
        Landmark lejontrappan = new Landmark(5, "Lejontrappan", new LatLng(57.706602, 11.967656));
        lejontrappan.setInfo("invigdes 1991");
        Landmark naturkompaniet = new Landmark(6, "Naturkompaniet", new LatLng(57.705866, 11.968216));
        naturkompaniet.setInfo("främjar om friluftsliv och hälsa");
        Landmark nk = new Landmark(7, "NK", new LatLng(57.705782, 11.969577));
        nk.setInfo("en scen för samtiden sedan 1902");
        Landmark mq = new Landmark(8, "MQ", new LatLng(57.706182, 11.970116));
        mq.setInfo("över 500 000 lojala kunder");
        Landmark zara = new Landmark(9, "Zara", new LatLng(57.706650, 11.969744));
        zara.setInfo("företaget är en del av Inditex");
        Landmark akademibokhandeln = new Landmark(10, "Akademibokhandeln", new LatLng(57.707287, 11.969581));
        akademibokhandeln.setInfo("ca 580 anställda i Sverige");

        shoppingLandmarks.add(carlings);
        shoppingLandmarks.add(wassens);
        shoppingLandmarks.add(nordstansguld);
        shoppingLandmarks.add(gustavadolf);
        shoppingLandmarks.add(lejontrappan);
        shoppingLandmarks.add(naturkompaniet);
        shoppingLandmarks.add(nk);
        shoppingLandmarks.add(mq);
        shoppingLandmarks.add(zara);
        shoppingLandmarks.add(akademibokhandeln);
    }

}