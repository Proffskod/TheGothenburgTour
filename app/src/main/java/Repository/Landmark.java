package Repository;

import android.graphics.Color;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A Landmark with a given attribute
 */
public class Landmark {

    private int landmarkId;
    private String title;
    private LatLng position;
    private Marker marker;
    private MarkerOptions markerOptions;
    private CircleOptions circleOptions;
    private Circle circle;
    private String info;

    /**
     * Initializes a Landmark with the given landmarkId, title and position.
     *
     * @param landmarkId this landmarks id
     * @param title this landmarks title
     * @param position this landmarks position
     */
    public Landmark(int landmarkId, String title, LatLng position) {
        this.landmarkId = landmarkId;
        this.title = title;
        this.position = position;
        this.circle = circle;
        markerOptions = new MarkerOptions();
        circleOptions = new CircleOptions();
        circleOptions.center(position).radius(25).fillColor(Color.argb(20, 10, 10, 10)).strokeColor(Color.TRANSPARENT);
    }

    public int getLandmarkId() {
        return landmarkId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public void setMarkerOptions(MarkerOptions markerOptions) {
        this.markerOptions = markerOptions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public Marker getMarker() {
        return marker;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public CircleOptions getCircleOptions() {
        return circleOptions;
    }

    /**
     * @return a String representation of a Landmark
     */
    @Override
    public String toString() {
        return "Landmark{" +
                "title='" + title + '\'' +
                ", position=" + position +
                ", markerOptions=" + markerOptions +
                ", circleOptions=" + circleOptions +
                '}';
    }
}
