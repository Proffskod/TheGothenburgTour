package Repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Mockup for Landmarks in Slottskogen quiz walk.
 */
public class LandmarkSlottskogenMockup implements LandmarkMockup {

    private ArrayList<Landmark> landmarks = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<Landmark> getLandmarks() {
        if (landmarks.isEmpty()) {
            createLandmarks();
        }
        return landmarks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createLandmarks() {
        Landmark linnePlatsen = new Landmark(1, "Linneplatsen", new LatLng(57.690385, 11.951338));
        linnePlatsen.setInfo("uppkom 1881");
        Landmark minigolf = new Landmark(2, "Äventyrsgolfbana", new LatLng(57.688914, 11.951254));
        minigolf.setInfo("en riktigt kul minigolf-upplevelse");
        Landmark polis = new Landmark(3, "Polisvakta", new LatLng(57.686769, 11.948054));
        polis.setInfo(" bemannad till mitten av 1950-talet");
        Landmark belParc = new Landmark(4, "Villa Belparc", new LatLng(57.683546, 11.947207));
        belParc.setInfo("Slottsskogens oas.");
        Landmark dammen = new Landmark(5, "Säldammen", new LatLng(57.683896, 11.942249));
        dammen.setInfo("här kan man skåda sälar");
        Landmark dalen = new Landmark(6, "Azeleadalen", new LatLng(57.684695, 11.935975));
        dalen.setInfo("här finns en barfotaslinga");
        Landmark hagarna = new Landmark(7, "Djurhagarna", new LatLng(57.687188, 11.942174));
        hagarna.setInfo("här finns bl.a älgar, hjortar m.m");
        Landmark barnZoo = new Landmark(8, "Barnens Zoo", new LatLng(57.685307, 11.942860));
        barnZoo.setInfo("här kan hela familjen klappa vänliga djur");
        Landmark plikta = new Landmark(9, "Plikta", new LatLng(57.689309, 11.946942));
        plikta.setInfo("Göteborgs första utflyktslekplats");
        Landmark museum = new Landmark(10, "Naturhistoriska", new LatLng(57.689859, 11.949785));
        museum.setInfo("Museet invigdes den 9 juli 1923");

        landmarks.add(linnePlatsen);
        landmarks.add(minigolf);
        landmarks.add(polis);
        landmarks.add(belParc);
        landmarks.add(dammen);
        landmarks.add(dalen);
        landmarks.add(hagarna);
        landmarks.add(barnZoo);
        landmarks.add(plikta);
        landmarks.add(museum);
    }

}