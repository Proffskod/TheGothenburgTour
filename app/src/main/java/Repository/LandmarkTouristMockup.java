package Repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Mockup for Landmarks in Tourist quiz walk.
 */
public class LandmarkTouristMockup implements LandmarkMockup {

    private ArrayList<Landmark> landmarks = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<Landmark> getLandmarks() {
        if (landmarks.isEmpty()) {
            createLandmarks();
        }
        return landmarks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createLandmarks() {
        Landmark liseberg = new Landmark(1, "Liseberg", new LatLng(57.697071, 11.990839));
        liseberg.setInfo("Nordens största nöjesfält");
        Landmark universeum = new Landmark(2, "Universeum", new LatLng(57.695839, 11.988419));
        universeum.setInfo("ett 10 000 kvadratmeter stort science center");
        Landmark gothia = new Landmark(3, "Gothia Towers", new LatLng(57.697216, 11.987609));
        gothia.setInfo("ett av Europas största hotell med 1200 rum.");
        Landmark scandinavium = new Landmark(4, "Scandinavium", new LatLng(57.699928, 11.986911));
        scandinavium.setInfo("12 044 personer får plats");
        Landmark ullevi = new Landmark(5, "Ullevi", new LatLng(57.705998, 11.985692));
        ullevi.setInfo(" Nordens största evenemangsarena.");
        Landmark heden = new Landmark(6, "Heden", new LatLng(57.702207, 11.978158));
        heden.setInfo("tidigare kallat Tegelbruksängen");
        Landmark foreningen = new Landmark(7, "Tradgårdsföreningen", new LatLng(57.703182, 11.972552));
        foreningen.setInfo("ett besöksmål för hela familjen");
        Landmark kungsport = new Landmark(8, "Kopparmärra", new LatLng(57.7044601, 11.9698359));
        kungsport.setInfo("ryttarstaty");
        Landmark rohsska = new Landmark(9, "Röhsska Museum", new LatLng(57.699957, 11.973089));
        rohsska.setInfo("öppnade 1916");
        Landmark gotaplatsen = new Landmark(10, "Götaplatsen", new LatLng(57.697238, 11.979512));
        gotaplatsen.setInfo("fick sitt namn 1914");
        Landmark test1 = new Landmark(11, "Kiosken", new LatLng(57.710598, 11.995037));
        test1.setInfo("nära Teknikhögskolan");
        Landmark test5 = new Landmark(12, "Dahls", new LatLng(57.710405, 11.996450));
        test5.setInfo("Bageri");

        landmarks.add(liseberg);
        landmarks.add(universeum);
        landmarks.add(gothia);
        landmarks.add(scandinavium);
        landmarks.add(ullevi);
        landmarks.add(heden);
        landmarks.add(foreningen);
        landmarks.add(kungsport);
        landmarks.add(rohsska);
        landmarks.add(gotaplatsen);
        landmarks.add(test1);
        landmarks.add(test5);
    }
}
