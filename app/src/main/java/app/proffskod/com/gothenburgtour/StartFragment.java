package app.proffskod.com.gothenburgtour;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * The first fragment that the application loads on start
 */
public class StartFragment extends Fragment {

    Button howToPlay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_startpage, container, false);
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.gothenburg));

        howToPlay = (Button) view.findViewById(R.id.howToPlay);
        howToPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HowToPlayFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, "howToPlay")
                        .commit();
            }
        });
        return view;
    }

}
