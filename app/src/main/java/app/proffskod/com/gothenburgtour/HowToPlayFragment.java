package app.proffskod.com.gothenburgtour;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A fragment that shows the user how the Application works
 */
public class HowToPlayFragment extends Fragment {

    /**
     * @param inflater the LayoutInflater
     * @param container the ViewGroup
     * @param savedInstanceState the Bundle
     * @returns the view of a layout for the fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.how_to_play_layout, container, false);

        return view;
    }
}
