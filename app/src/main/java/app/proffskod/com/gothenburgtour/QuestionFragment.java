package app.proffskod.com.gothenburgtour;

import Repository.Landmark;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Fragment for the Questions
 */
public class QuestionFragment extends Fragment {

    private View view;
    private Button button1;
    private Button buttonX;
    private Button button2;
    private TextView fragment_question;
    private int selectedAnswer;
    Bundle questionBundle;
    FragmentActivity fragmentActivity;

    @Override
    public void onAttach(Activity activity) {
        fragmentActivity = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        questionBundle = savedInstanceState;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_question, container, false);
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.gothenburg));
        String question = getArguments().getString("question");
        final String[] answers = getArguments().getStringArray("answers");
        final int rightAnswer = getArguments().getInt("rightAnswer");

        // get the question from that landmark fragment
        fragment_question = (TextView) view.findViewById(R.id.question);
        fragment_question.setText(question);

        button1 = (Button) view.findViewById(R.id.button1);
        button1.setText(answers[0]);
        buttonX = (Button) view.findViewById(R.id.buttonX);
        buttonX.setText(answers[1]);
        button2 = (Button) view.findViewById(R.id.button2);
        button2.setText(answers[2]);

        // check with right answer if its the right answer
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = 1;
                if (rightAnswer == 1) {
                    answerResult(answers[0]);
                } else if (rightAnswer == 2) {
                    answerResult(answers[1]);
                } else if (rightAnswer == 3) {
                    answerResult(answers[2]);
                }
            }
        });

        buttonX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = 2;
                if (rightAnswer == 2) {
                    answerResult(answers[1]);
                } else if (rightAnswer == 1) {
                    answerResult(answers[0]);
                } else if (rightAnswer == 3) {
                    answerResult(answers[2]);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = 3;
                if (rightAnswer == 3) {
                    answerResult(answers[2]);
                } else if (rightAnswer == 1) {
                    answerResult(answers[0]);
                } else if (rightAnswer == 2) {
                    answerResult(answers[1]);
                }
            }
        });
        return view;
    }

    /**
     * Gives the user a response depending on the answer
     *
     * @param answer
     */
    private void answerResult(String answer) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);

        if (selectedAnswer == getArguments().getInt("rightAnswer")) {
            builder.setTitle("RÄTT!").setMessage("Du svarade:\n" + answer).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
                            fragmentManager.popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    });
        } else {
            builder.setTitle("FEL!").setMessage("Rätt svar är:\n" + answer).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
                            fragmentManager.popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    });
        }

        AlertDialog dialog = builder.show();
        TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.LEFT);
        messageText.setTextSize(20);
        dialog.setView(messageText);
        dialog.setCancelable(false);
        messageText.setTextColor(getResources().getColor(R.color.silver));
        dialog.show();
    }

    public int getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(int selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }
}