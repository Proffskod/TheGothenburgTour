package app.proffskod.com.gothenburgtour;

import Repository.Landmark;
import Repository.LandmarkSlottskogenMockup;
import Repository.Question;
import Repository.QuestionsSlottskogenMockup;
import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Fragment for SlottskogenQuiz
 */
public class SlottskogenQuiz extends Fragment
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, com.google.android.gms.location.LocationListener {

    private LandmarkSlottskogenMockup landmarkSlottskogenMockup = new LandmarkSlottskogenMockup();
    private QuestionsSlottskogenMockup questionsSlottskogenMockup = new QuestionsSlottskogenMockup();
    private ArrayList<Landmark> landmarks = landmarkSlottskogenMockup.getLandmarks();
    private FileInputStream fileInputStream;
    private FileOutputStream fileOutputStream;
    private int correctAnswers = 0;
    private int wrongAnswers = 0;
    private static final String TAG = SlottskogenQuiz.class.getSimpleName();

    // Strings for file names
    private static final String RIGHT_SCORE = TAG + "right";
    private static final String WRONG_SCORE = TAG + "wrong";
    private static final String CORRECT_LANDMARKS = TAG + "correctLandmarks";
    private static final String WRONG_LANDMARKS = TAG + "wrongLandmarks";

    private Button reset;
    private ArrayList<String> correctLandmarks = new ArrayList<>();
    private ArrayList<String> wrongLandmarks = new ArrayList<>();
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient googleApiClient;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private MapView mapView;
    private GoogleMap map;
    private String provider;
    private View view;
    private TextView correct;
    private TextView wrong;
    private AlertDialog dialog;
    FragmentActivity myContext;
    QuestionFragment questionFragment;
    Question question;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.slottskogen));

        view = inflater.inflate(R.layout.fragment_tour, container, false);

        Criteria criteria = new Criteria();
        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(criteria, false);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        map = mapView.getMap();

        // creates files if they don't already exists
        if (new File(view.getContext().getFilesDir(), RIGHT_SCORE).exists()
                && new File(view.getContext().getFilesDir(), WRONG_SCORE).exists()
                && new File(view.getContext().getFilesDir(), CORRECT_LANDMARKS).exists()
                && new File(view.getContext().getFilesDir(), WRONG_LANDMARKS).exists()) {
            correctAnswers = Integer.parseInt(readRightScoreFile());
            wrongAnswers = Integer.parseInt(readWrongScoreFile());
            correctLandmarks = loadCorrectLandmarks();
            wrongLandmarks = loadWrongLandmarks();
        } else {
            writeRightScoreFile(String.valueOf(correctAnswers));
            writeWrongScoreFile(String.valueOf(wrongAnswers));
            writeCorrectLandmarks(correctLandmarks);
            writeWrongLandmarks(wrongLandmarks);
        }


        wrong = (TextView) view.findViewById(R.id.wrong_answers);
        wrong.setText(String.valueOf(wrongAnswers));
        correct = (TextView) view.findViewById(R.id.correct_answers);
        correct.setText(String.valueOf(correctAnswers));

        reset = (Button) view.findViewById(R.id.reboot);

        // Resets scores and visited landmarks on click
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCorrectAnswers(0);
                writeRightScoreFile(String.valueOf(getCorrectAnswers()));
                correct.setText(String.valueOf(getCorrectAnswers()));
                setWrongAnswers(0);
                writeWrongScoreFile(String.valueOf(getWrongAnswers()));
                wrong.setText(String.valueOf(getWrongAnswers()));
                wrongLandmarks.removeAll(wrongLandmarks);
                writeWrongLandmarks(wrongLandmarks);
                correctLandmarks.removeAll(correctLandmarks);
                writeCorrectLandmarks(correctLandmarks);
                resetLandmarks(landmarks);
            }
        });

        setMapView();
        buildGoogleApiClient();

        // makes a location request and set the desired and fastest interval a for active location updates, in milliseconds.
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000).setFastestInterval(10 * 1000);


        DirectionsFetcher df = new DirectionsFetcher();
        df.execute();

        return view;
    }

    /**
     * Sets the mapview to the fragment.
     */
    private void setMapView() {
        try {
            MapsInitializer.initialize(getActivity());
            switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    if (mapView != null) {
                        Log.v("msg", "mapview not empty");
                        locationManager = ((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE));
                        map = mapView.getMap();
                        if (map == null) {
                            Log.d("", "Map Fragment Not Found or no Map in it!!");
                        }
                        map.clear();
                        try {
                            addMarkers(map);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        map.setIndoorEnabled(true);
                        map.setMyLocationEnabled(true);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:
                    Toast.makeText(view.getContext(), "Service is missing", Toast.LENGTH_SHORT);
                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                    Toast.makeText(view.getContext(), "Update required", Toast.LENGTH_SHORT);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds all Landmark markers to the Google Map
     *
     * @param map
     */
    private void addMarkers(GoogleMap map) {
        for (Landmark l : landmarks) {
            map.addMarker(l.getMarkerOptions()
                    .position(l.getPosition())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    .title(l.getTitle())
                    .snippet(l.getInfo()));
            l.setCircle(map.addCircle(l.getCircleOptions()));
        }
    }

    /**
     * Gets the last location and zooms into that location
     *
     * @param bundle
     */
    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else {
            zoomInOnMyLocation(location);
            handleNewLocation(location);
        }
    }

    /**
     * Resets all visited landmarks
     *
     * @param landmarks
     */
    private void resetLandmarks(ArrayList<Landmark> landmarks) {
        for (Landmark l : landmarks) {
            l.getCircle().setFillColor(Color.argb(20, 10, 10, 10));
        }
    }

    /**
     * Builder to configure a GoogleApiClient.
     */
    protected final synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Handles every location.
     * Checks if the user is inside a circle or not.
     *
     * @param location
     */
    private void handleNewLocation(Location location) {
        float[] distance = new float[2];
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        for (Landmark l : landmarks) {
            Location.distanceBetween(currentLatitude, currentLongitude,
                    l.getCircleOptions().getCenter().latitude,
                    l.getCircleOptions().getCenter().longitude, distance);

            if (correctLandmarks.contains(l.getTitle())) {
                l.getCircle().setFillColor(Color.argb(70, 51, 255, 102));
            } else if (wrongLandmarks.contains(l.getTitle())) {
                l.getCircle().setFillColor(Color.argb(70, 255, 99, 71));
            }

            if (distance[0] < l.getCircleOptions().getRadius()
                    && l.getCircle().getFillColor() != Color.argb(70, 51, 255, 102)
                    && l.getCircle().getFillColor() != Color.argb(70, 255, 99, 71)) {

                // stop checking for location while user is in a question fragment
                onPause();
                onStop();
                sendNotification(l);

                question = questionsSlottskogenMockup.getQuestionById(l.getLandmarkId());
                Bundle bundle = new Bundle();
                bundle.putString("question", question.getQuestion());
                bundle.putStringArray("answers", question.getAnswers());
                bundle.putInt("rightAnswer", question.getRightAnswer());

                questionFragment = new QuestionFragment();
                questionFragment.setArguments(bundle);
                FragmentManager fragmentManager = myContext.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack if needed
                transaction.add(R.id.container, questionFragment).addToBackStack(null).commit();

                CheckAnswer checkAnswer = new CheckAnswer();
                checkAnswer.execute(l);
            }
        }
    }

    /**
     * Zooms into the given location
     *
     * @param location
     */
    private void zoomInOnMyLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location Services Suspended to Service, please reconnect");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location Service Connection Failed with Code " +
                    connectionResult.getErrorCode());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();
        correctAnswers = Integer.parseInt(readRightScoreFile());
        wrongAnswers = Integer.parseInt(readWrongScoreFile());
        correctLandmarks = loadCorrectLandmarks();
        wrongLandmarks = loadWrongLandmarks();
        locationManager.requestLocationUpdates(provider, 1000, 1, this);
    }

    /**
     * Saves visited landmarks and score when the application is paused.
     * It will also close the connection to Google Play services.
     */
    @Override
    public void onPause() {
        super.onPause();
        writeWrongLandmarks(wrongLandmarks);
        writeCorrectLandmarks(correctLandmarks);
        writeRightScoreFile(String.valueOf(correctAnswers));
        writeWrongScoreFile(String.valueOf(wrongAnswers));
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    /**
     * Receiving notifications from the LocationManager when the location has changed
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    /**
     * Called when the provider is disabled by the user
     * and asks the user to turn on the GPS
     *
     * @param provider
     */
    @Override
    public void onProviderDisabled(String provider) {
        Activity activity = getActivity();
        if (activity != null && isAdded()) {
            Toast.makeText(view.getContext(), "Please turn on your gps", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
        }
    }

    /**
     * Sends notification to the user
     *
     * @param l
     */
    public void sendNotification(Landmark l) {
        Intent intent = new Intent(view.getContext(), TouristQuiz.class);
        PendingIntent pIntent = PendingIntent.getActivity(view.getContext(), 0, intent, 0);

        Notification n = new Notification.Builder(view.getContext())
                .setContentTitle(l.getTitle())
                .setContentText("You are near a question!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true).build();

        NotificationManager notificationManager =
                (NotificationManager) view.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
    }

    /**
     * Checks the answer and handles the result
     */
    protected class CheckAnswer extends AsyncTask<Landmark, Integer, Boolean> {
        Landmark landmark;
        boolean b;

        /**
         * Is running as long as the user hasn't answered the question
         *
         * @param params
         * @return true or false depending on the answer
         */
        @Override
        protected Boolean doInBackground(Landmark... params) {
            for (Landmark l : params) {
                landmark = l;
                do {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (questionFragment.getSelectedAnswer() == question.getRightAnswer()) {
                        b = true;
                    } else if (questionFragment.getSelectedAnswer() != question.getRightAnswer()) {
                        b = false;
                    }
                } while (questionFragment.getSelectedAnswer() == 0);
            }
            return b;
        }

        /**
         * Executes what happens when the user has answered
         * Depending on if the answer was correct or not it will set the circle color,
         * add score and save the landmark.
         *
         * @param aBoolean
         */
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean == true) {
                landmark.getCircle().setFillColor(Color.argb(70, 51, 255, 102));
                setCorrectAnswers(getCorrectAnswers() + 1);
                correct.setText(String.valueOf(correctAnswers));
                writeRightScoreFile(String.valueOf(getCorrectAnswers()));
                correctLandmarks.add(landmark.getTitle());
                writeCorrectLandmarks(correctLandmarks);
            } else if (aBoolean == false) {
                landmark.getCircle().setFillColor(Color.argb(70, 255, 99, 71));
                setWrongAnswers(getWrongAnswers() + 1);
                wrong.setText(String.valueOf(wrongAnswers));
                writeWrongScoreFile(String.valueOf(getWrongAnswers()));
                wrongLandmarks.add(landmark.getTitle());
                writeWrongLandmarks(wrongLandmarks);
            }
            questionFragment.setSelectedAnswer(0);
            // starts checking for new locations again
            onResume();
            onStart();

            checkSummary();
        }
    }

    /**
     * Saves the amount of right answers to a specified file
     *
     * @param points
     */
    public void writeRightScoreFile(String points) {
        fileOutputStream = null;
        try {
            fileOutputStream = view.getContext().openFileOutput(RIGHT_SCORE, Context.MODE_PRIVATE);
            fileOutputStream.write(points.getBytes());
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found");
        } catch (IOException e) {
            Log.d(TAG, "IO Error");
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                Log.d(TAG, "IO ERROR");
            }
        }
    }

    /**
     * Saves the amount of wrong answers to a specified file
     *
     * @param points
     */
    public void writeWrongScoreFile(String points) {
        fileOutputStream = null;
        try {
            fileOutputStream = view.getContext().openFileOutput(WRONG_SCORE, Context.MODE_PRIVATE);
            fileOutputStream.write(points.getBytes());
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found");
        } catch (IOException e) {
            Log.d(TAG, "IO Error");
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                Log.d(TAG, "IO ERROR");
            }
        }
    }

    /**
     * Saves the landmarks which were answered wrong to a specified file
     *
     * @param wrongLandmarks
     */
    public void writeWrongLandmarks(ArrayList<String> wrongLandmarks) {
        try {
            fileOutputStream = view.getContext().openFileOutput(WRONG_LANDMARKS, Context.MODE_PRIVATE);
            for (String s : wrongLandmarks) {
                fileOutputStream.write(s.getBytes());
                fileOutputStream.write(System.getProperty("line.separator").getBytes());
            }
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the landmarks which were answered right from file
     *
     * @return landmarks which were answered correct
     */
    public ArrayList<String> loadCorrectLandmarks() {
        ArrayList<String> correctLandmarks = new ArrayList<>();
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferedReader;
        String line;
        try {
            inputStream = view.getContext().openFileInput(CORRECT_LANDMARKS);
            inputReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputReader);
            while ((line = bufferedReader.readLine()) != null) {
                correctLandmarks.add(line);
            }
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.i(TAG, e.getMessage());
            }
        }
        return correctLandmarks;
    }

    /**
     * Saves the landmarks which were answered right to a specified file
     *
     * @param correctLandmarks
     */
    public void writeCorrectLandmarks(ArrayList<String> correctLandmarks) {
        try {
            fileOutputStream = view.getContext().openFileOutput(CORRECT_LANDMARKS, Context.MODE_PRIVATE);
            for (String s : correctLandmarks) {
                fileOutputStream.write(s.getBytes());
                fileOutputStream.write(System.getProperty("line.separator").getBytes());
            }
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the landmarks which were answered wrong from file
     *
     * @return landmarks which were answered wrong
     */
    public ArrayList<String> loadWrongLandmarks() {
        ArrayList<String> wrongLandmarks = new ArrayList<>();
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferedReader;
        String line;
        try {
            inputStream = view.getContext().openFileInput(WRONG_LANDMARKS);
            inputReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputReader);
            while ((line = bufferedReader.readLine()) != null) {
                wrongLandmarks.add(line);
            }
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.i(TAG, e.getMessage());
            }
        }
        return wrongLandmarks;
    }


    /**
     * @return the amount of correct answers from file
     */
    public String readRightScoreFile() {
        fileInputStream = null;
        String file = null;
        try {
            fileInputStream = view.getContext().openFileInput(RIGHT_SCORE);
            int size = fileInputStream.available();
            byte[] buffer = new byte[size];
            fileInputStream.read(buffer);
            fileInputStream.close();
            file = new String(buffer, "UTF-8");
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found");
        } catch (Exception e) {
            Log.d(TAG, "Error");
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                Log.d(TAG, "IO Error");
            }
        }
        return file;
    }

    /**
     * @return the amount of wrong answers from file
     */
    public String readWrongScoreFile() {
        fileInputStream = null;
        String file = null;
        try {
            fileInputStream = view.getContext().openFileInput(WRONG_SCORE);
            int size = fileInputStream.available();
            byte[] buffer = new byte[size];
            fileInputStream.read(buffer);
            fileInputStream.close();
            file = new String(buffer, "UTF-8");
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found");
        } catch (Exception e) {
            Log.d(TAG, "Error");
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                Log.d(TAG, "IO Error");
            }
        }
        return file;
    }

    /**
     * Calls the Google Maps Direction API to the the directions between each Landmark
     */
    private class DirectionsFetcher extends AsyncTask<URL, Integer, String> {

        private List<LatLng> latLngs = new ArrayList<LatLng>();
        private URL url;
        private HttpURLConnection httpURLConnection;
        private BufferedReader reader;
        private String httpUrlResponse;

        /**
         * Parses the Json response
         *
         * @param urlResponse
         * @return the locations
         * @throws JSONException
         */
        private List<LatLng> getLatLngs(String urlResponse) throws JSONException {
            final String START_LOCATION = "start_location";
            final String END_LOCATION = "end_location";
            final String LAT = "lat";
            final String LNG = "lng";

            JSONObject response = new JSONObject(urlResponse);
            JSONArray waypointsLatLng = response.getJSONArray("routes");
            waypointsLatLng = waypointsLatLng.getJSONObject(0).getJSONArray("legs");

            for (int i = 0; i < waypointsLatLng.length(); i++) {
                JSONObject waypoint = waypointsLatLng.getJSONObject(i);
                JSONArray waypointArray = waypoint.getJSONArray("steps");
                LatLng mark = new LatLng(
                        waypoint.getJSONObject(START_LOCATION).getDouble(LAT),
                        waypoint.getJSONObject(START_LOCATION).getDouble(LNG));
                latLngs.add(mark);
                for (int j = 0; j < waypointArray.length(); j++) {
                    LatLng point = new LatLng(
                            waypointArray.getJSONObject(j).getJSONObject(START_LOCATION).getDouble(LAT),
                            waypointArray.getJSONObject(j).getJSONObject(START_LOCATION).getDouble(LNG));
                    LatLng point2 = new LatLng(
                            waypointArray.getJSONObject(j).getJSONObject(END_LOCATION).getDouble(LAT),
                            waypointArray.getJSONObject(j).getJSONObject(END_LOCATION).getDouble(LNG));
                    latLngs.add(point);
                    latLngs.add(point2);
                }
                LatLng mark2 = new LatLng(
                        waypoint.getJSONObject(END_LOCATION).getDouble(LAT),
                        waypoint.getJSONObject(END_LOCATION).getDouble(LNG));
                latLngs.add(mark2);
            }
            return latLngs;
        }

        /**
         * Creates a connection to the Google Maps Directions API and saves the response
         *
         * @param params
         * @return the response from the Google Maps Directions API
         */
        @Override
        protected String doInBackground(URL... params) {
            for (int i = 0; i < landmarks.size(); ++i) {
                Landmark landmark = landmarks.get(i);
                Landmark landmark1;
                String line;

                if (i == landmarks.size() - 1) {
                    landmark1 = landmarks.get(0);
                } else {
                    landmark1 = landmarks.get(i + 1);
                }
                try {
                    url = new URL("https://maps.googleapis.com/maps/api/directions/json?origin="
                            + landmark.getPosition().latitude + ","
                            + landmark.getPosition().longitude + "&destination="
                            + landmark1.getPosition().latitude + ","
                            + landmark1.getPosition().longitude
                            + "&mode=walking&key=AIzaSyAOTZwxuItP6AstWKzeC_l5ufDpFxMaJuM");

                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.connect();

                    InputStream inputStream = httpURLConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if (inputStream == null) {
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line + "\n");
                    }
                    if (buffer.length() == 0) {
                        return null;
                    }
                    httpUrlResponse = buffer.toString();
                    try {
                        getLatLngs(httpUrlResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        /**
         * Sets a polyline between the locations
         *
         * @param result
         */
        @Override
        protected void onPostExecute(String result) {
            map.addPolyline(new PolylineOptions().addAll(latLngs).color(Color.argb(40, 33, 150, 243)));
        }
    }

    /**
     * Checks the amount of total correct and wrong answers
     * and provides a response to the user depending on how many correct answers
     */
    public void checkSummary() {
        FragmentManager fragmentManager = myContext.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(questionFragment).commit();
        if (correctAnswers + wrongAnswers == 10) {
            dialog = new AlertDialog.Builder(myContext, AlertDialog.THEME_DEVICE_DEFAULT_DARK).create();
            dialog.setTitle("Du har gått klart tipsprommenaden!");
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            switch (correctAnswers) {
                case 1:
                    dialog.setMessage("Ett rätt, men en redig prommenad!");
                    dialog.show();
                    break;
                case 2:
                    dialog.setMessage("Två rätt är mer än inga rätt alls! Ett tips är att titta efter informationsskyltar");
                    dialog.show();
                    break;
                case 3:
                    dialog.setMessage("Tre rätt, det är väl inte så pjåkigt!");
                    dialog.show();
                    break;
                case 4:
                    dialog.setMessage("Så nära till hälften rätt man kan komma!");
                    dialog.show();
                    break;
                case 5:
                    dialog.setMessage("Hälften rätt, inte illa!");
                    dialog.show();
                    break;
                case 6:
                    dialog.setMessage("Lite mer än hälften, bra jobbat!");
                    dialog.show();
                    break;
                case 7:
                    dialog.setMessage("Lite mer än hälften, bra jobbat!");
                    dialog.show();
                    break;
                case 8:
                    dialog.setMessage("Lite mer än hälften, bra jobbat!");
                    dialog.show();
                    break;
                case 9:
                    dialog.setMessage("Nästan alla rätt, men nära skjuter ingen hare!");
                    dialog.show();
                    break;
                case 10:
                    dialog.setMessage("Du kan minsann Slottskogen och lite till!");
                    dialog.show();
                    break;
            }
        }
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public int getWrongAnswers() {
        return wrongAnswers;
    }

    public void setWrongAnswers(int wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

}
