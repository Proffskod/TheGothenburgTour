package app.proffskod.com.gothenburgtour;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by ForsmanFredrik on 2015-05-25.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class MyActivityTest {

    private MainActivity mainActivity;
    private NavigationDrawerFragment fragment;
    private Button button;
    public static final String BUTTON_TEXT = "How to play";

    @Before
    public void setUp() throws Exception {
        mainActivity = Robolectric.buildActivity(MainActivity.class).attach().create().start().resume().get();
        fragment = (NavigationDrawerFragment) mainActivity.getSupportFragmentManager().findFragmentById(R.layout.fragment_navigation_drawer);
        button = (Button) mainActivity.findViewById(R.id.howToPlay);
    }

    @Test
    public void fragmentContainer() throws Exception {
        View view = mainActivity.findViewById(R.id.container);
        assertNotNull("The tablet cointainer does not exist", view);
    }

    @Test
    public void shouldHaveLayout() throws Exception {
        assertNotNull(mainActivity.drawerLayout);
    }

    @Test
    public void shouldHaveFragment() throws Exception {
        assertNotNull(mainActivity.getSupportFragmentManager());

    }

    @Test
    public void shouldBeSameFragment() throws Exception {
        Fragment fragmentById = mainActivity.getSupportFragmentManager()
                .findFragmentById(R.layout.fragment_navigation_drawer);
        assertSame(fragment, fragmentById);
    }

    @Test
    public void actionBarTests() throws Exception {

        ActionBar actionBar = mainActivity.getSupportActionBar();
        assertTrue("Actionbar is not shown", actionBar.isShowing());
        assertEquals("ActionBar title is incorrect",
                mainActivity.getString(R.string.gothenburg),
                actionBar.getTitle());
    }

    @Test
    public void navigationDrawerOnSelect() throws Exception {
        mainActivity.onNavigationDrawerItemSelected(0);
        StartFragment startFragment = (StartFragment) mainActivity
                .getSupportFragmentManager()
                .findFragmentByTag("fragment");
        assertNotNull("Fragment should have existed", startFragment);

    }

    @Test
    public void imageViewSrcIsCorrectDrawable() throws Exception{
        ImageView imageView = (ImageView)mainActivity.findViewById(R.id.backgroundHome);
        Drawable drawable = mainActivity.getResources().getDrawable(R.drawable.gbglogga);
        assertEquals(imageView.getDrawable(), drawable);
    }

    @Test
    public void testHowToPlayButton() throws Exception {

        assertTrue(button.hasOnClickListeners());
        button.performClick();
        Fragment frag = mainActivity.getSupportFragmentManager().findFragmentByTag("howToPlay");
        Assert.assertNotNull(frag);

        assertNotNull(button);
        assertEquals(BUTTON_TEXT, button.getText().toString());
    }
}
